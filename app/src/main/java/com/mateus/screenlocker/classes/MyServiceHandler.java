package com.mateus.screenlocker.classes;

import android.content.Context;
import android.content.Intent;

import com.mateus.screenlocker.receivers.MyService;

/**
 * Created by root on 07/07/17.
 */

public class MyServiceHandler {
    public void start(Context context) {
        context.startService(new Intent(context, MyService.class));
    }
    public void stop(Context context) {
        context.stopService(new Intent(context, MyService.class));
    }
}

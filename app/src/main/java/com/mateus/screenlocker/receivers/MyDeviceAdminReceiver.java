package com.mateus.screenlocker.receivers;

import android.app.admin.DeviceAdminReceiver;
import android.content.Context;
import android.content.Intent;

import com.mateus.screenlocker.R;
import com.mateus.screenlocker.classes.MyServiceHandler;

/**
 * Created by root on 01/07/17.
 */

public class MyDeviceAdminReceiver extends DeviceAdminReceiver {
    @Override
    public void onEnabled(Context context, Intent intent) {
        super.onEnabled(context, intent);
        new MyServiceHandler().start(context);
    }

    @Override
    public void onDisabled(Context context, Intent intent) {
        super.onDisabled(context, intent);
        new MyServiceHandler().stop(context);
    }

    @Override
    public CharSequence onDisableRequested(Context context, Intent intent) {
        return context.getString(R.string.deactivate_device_adm_message);
    }
}

package com.mateus.screenlocker.receivers;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.app.TaskStackBuilder;
import android.app.admin.DevicePolicyManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Build;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.PowerManager;
import android.os.Process;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;

import com.mateus.screenlocker.R;
import com.mateus.screenlocker.activities.MainActivity;

import static com.mateus.screenlocker.activities.MainActivity.sComponentName;
import static com.mateus.screenlocker.activities.MainActivity.sDevicePolicyManager;

/**
 * Created by root on 01/07/17.
 */

public class MyService extends Service implements SensorEventListener {
    private Looper mServiceLooper;
    private ServiceHandler mServiceHandler;

    private SensorManager mSensorManager;
    private Sensor mSensor;
    private PowerManager.WakeLock mScreenDimWakeLock;

    private float[] mGravity;
    private float[] mLinearAcceleration;
    private byte[] mShakes;
    private long mLastUpdate;

    public volatile static boolean sIsRunning;
    public volatile static int sMaxNumberOfShakes = 4; // 4 shakes to change screen power
    public volatile static int sMaxAccelerationDuringShake = 5; // 5 meters/s²
    public volatile static boolean sIsXEnabled = true;
    public volatile static boolean sIsYEnabled;
    public volatile static boolean sIsZEnabled;

    private static final float ALPHA = (float) 0.8;
    private static final long MAX_TIME_BETWEEN_SHAKES = 4 * 1000; // 4 seconds clear shakes
    private static final long MIN_TIME_BETWEEN_SHAKES = 500; // 200 milli time to count shakes
    private static final int NOTIFICATION_ID = 1;
    private static final int ONGOING_NOTIFICATION_ID = 2;
    private static final int MESSAGE_WHAT_CODE = 1;

    // Handler that receives messages from the thread
    private final class ServiceHandler extends Handler {

        ServiceHandler(Looper looper) {
            super(looper);
        }

        @Override
        public void handleMessage(Message msg) {
            handleSensorEvent((SensorEvent) msg.obj);
        }

    }

    public BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (Intent.ACTION_SCREEN_OFF.equalsIgnoreCase(intent.getAction())) {
                Runnable runnable = new Runnable() {
                    public void run() {
                        unregisterListener();
                        registerListener();
                    }
                };
                new Handler().postDelayed(runnable, 500);
            }
        }
    };

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        sIsRunning = true;

        mGravity = new float[3];
        mLinearAcceleration = new float[3];
        mShakes = new byte[3];

        getWakeLock();
        startForeground();

        // Register our receiver for the ACTION_SCREEN_OFF action. This will make our receiver
        // code be called whenever the phone enters standby mode.
        registerReceiver(mReceiver, new IntentFilter(Intent.ACTION_SCREEN_OFF));

        getDefaultSensor();
        registerListener();

        // Start up the thread running the service.  Note that we create a
        // separate thread because the service normally runs in the process's
        // main thread, which we don't want to block.  We also make it
        // background priority so CPU-intensive work will not disrupt our UI.
        HandlerThread thread = new HandlerThread("HandlerThread", Process.THREAD_PRIORITY_BACKGROUND);
        thread.start();

        // Get the HandlerThread's Looper and use it for our Handler
        mServiceLooper = thread.getLooper();
        mServiceHandler = new ServiceHandler(mServiceLooper);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        unregisterListener();
        unregisterReceiver(mReceiver);
        stopForeground(true);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
            mServiceLooper.quitSafely();
        } else {
            mServiceLooper.quit();
        }

        if (mScreenDimWakeLock.isHeld()) {
            mScreenDimWakeLock.release();
        }
        sIsRunning = false;
    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        Message msg = mServiceHandler.obtainMessage();
        msg.obj = sensorEvent;
        msg.what = MESSAGE_WHAT_CODE;
        mServiceHandler.sendMessage(msg);
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }

    private synchronized void handleSensorEvent(SensorEvent sensorEvent) {
        long currentTimeMillis = System.currentTimeMillis();
        long delta = currentTimeMillis - mLastUpdate;
        if (sensorEvent.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
            if (delta > MAX_TIME_BETWEEN_SHAKES) {
                mLastUpdate = currentTimeMillis;
                mShakes[0] = mShakes[1] = mShakes[2] = 0;
            } else if (delta > MIN_TIME_BETWEEN_SHAKES) {
                computeAcceleration(sensorEvent);
                verifyShake();
            }
        }
    }

    private void getDefaultSensor() {
        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        mSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
    }

    private void registerListener() {
        mSensorManager.registerListener(this, mSensor, SensorManager.SENSOR_DELAY_NORMAL);
    }

    private void unregisterListener() {
        mSensorManager.unregisterListener(this);
    }

    private void getWakeLock() {
        PowerManager powerManager = (PowerManager) getSystemService(Context.POWER_SERVICE);
        mScreenDimWakeLock = powerManager.newWakeLock(PowerManager.SCREEN_DIM_WAKE_LOCK | PowerManager.ACQUIRE_CAUSES_WAKEUP, "mScreenDimWakeLock");
        mScreenDimWakeLock.acquire();
    }

    private void computeAcceleration(SensorEvent sensorEvent) {
        // In this example, ALPHA is calculated as t / (t + dT),
        // where t is the low-pass filter's time-constant and
        // dT is the event delivery rate.

        // Isolate the force of mGravity with the low-pass filter.
        mGravity[0] = ALPHA * mGravity[0] + (1 - ALPHA) * sensorEvent.values[0];
        mGravity[1] = ALPHA * mGravity[1] + (1 - ALPHA) * sensorEvent.values[1];
        mGravity[2] = ALPHA * mGravity[2] + (1 - ALPHA) * sensorEvent.values[2];

        // Remove the mGravity contribution with the high-pass filter.
        mLinearAcceleration[0] = sensorEvent.values[0] - mGravity[0];
        mLinearAcceleration[1] = sensorEvent.values[1] - mGravity[1];
        mLinearAcceleration[2] = sensorEvent.values[2] - mGravity[2];
    }

    private void verifyShake() {
        if (sIsXEnabled && Math.abs(mLinearAcceleration[0]) > sMaxAccelerationDuringShake) {
            mShakes[0]++;
        }
        if (sIsYEnabled && Math.abs(mLinearAcceleration[1]) > sMaxAccelerationDuringShake) {
            mShakes[1]++;
        }
        if (sIsZEnabled && Math.abs(mLinearAcceleration[2]) > sMaxAccelerationDuringShake) {
            mShakes[2]++;
        }
        for (byte shake : mShakes) {
            if (shake >= sMaxNumberOfShakes) {
                mServiceHandler.removeMessages(MESSAGE_WHAT_CODE);
                changeScreenPower(isScreenOn());
                mShakes[0] = mShakes[1] = mShakes[2] = 0;
                mLinearAcceleration[0] = mLinearAcceleration[1] = mLinearAcceleration[2] = 0;
                break;
            }
        }
    }

    private boolean isScreenOn() {
        PowerManager powerManager = (PowerManager) getSystemService(POWER_SERVICE);
        return android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT_WATCH ? powerManager.isInteractive() : powerManager.isScreenOn();
    }

    private void changeScreenPower(boolean isScreenOn) {
        if (isScreenOn) {
            lockScreenIfAdmin();
        } else {
            if (mScreenDimWakeLock.isHeld()) {
                mScreenDimWakeLock.release();
            }
            mScreenDimWakeLock.acquire();
        }
    }

    private void lockScreenIfAdmin() {
        if (sDevicePolicyManager == null) {
            sDevicePolicyManager = (DevicePolicyManager) getSystemService(Context.DEVICE_POLICY_SERVICE);
            sComponentName = new ComponentName(this, MyDeviceAdminReceiver.class);
        }
        if (sDevicePolicyManager.isAdminActive(sComponentName)) {
            sDevicePolicyManager.lockNow();
        } else {
            showNoPermissionNotification();
        }
    }

    private void showNoPermissionNotification() {
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setContentTitle(getString(R.string.app_name))
                        .setContentText(getString(R.string.app_needs_permission))
                        .setDefaults(NotificationCompat.DEFAULT_ALL)
                        .setPriority(NotificationCompat.PRIORITY_HIGH)
                        .setAutoCancel(true);
        // Creates an explicit intent for an Activity in your app
        Intent resultIntent = new Intent(this, MainActivity.class);

        // The stack builder object will contain an artificial back stack for the
        // started Activity.
        // This ensures that navigating backward from the Activity leads out of
        // your application to the Home screen.
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        // Adds the back stack for the Intent (but not the Intent itself)
        stackBuilder.addParentStack(MainActivity.class);
        // Adds the Intent that starts the Activity to the top of the stack
        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
        mBuilder.setContentIntent(resultPendingIntent);
        NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        // NOTIFICATION_ID allows you to update the notification later on.
        mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());
    }

    private void startForeground() {
        Intent notificationIntent = new Intent(this, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, notificationIntent, 0);
        Notification notification = new Notification.Builder(this)
                .setContentTitle(getText(R.string.app_name))
                .setContentText(getText(R.string.activated))
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentIntent(pendingIntent)
                .setTicker(getText(R.string.activated))
                .setWhen(System.currentTimeMillis())
                .setOngoing(true)
                .build();
        startForeground(ONGOING_NOTIFICATION_ID, notification);
    }
}
package com.mateus.screenlocker.activities;

import android.app.admin.DevicePolicyManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.hrules.horizontalnumberpicker.HorizontalNumberPicker;
import com.hrules.horizontalnumberpicker.HorizontalNumberPickerListener;
import com.mateus.screenlocker.R;
import com.mateus.screenlocker.classes.MyServiceHandler;
import com.mateus.screenlocker.receivers.MyDeviceAdminReceiver;
import com.mateus.screenlocker.receivers.MyService;

/**
 * Created by root on 30/06/17.
 */

public class MainActivity extends AppCompatActivity implements View.OnClickListener, CompoundButton.OnCheckedChangeListener {
    private FloatingActionButton mFAB;
    private TextView mTextViewSensibility;
    private SeekBar mSeekBar;
    private HorizontalNumberPicker mNumberPicker;
    private CheckBox mCheckBoxX;
    private CheckBox mCheckBoxY;
    private CheckBox mCheckBoxZ;
    public static DevicePolicyManager sDevicePolicyManager;
    public static ComponentName sComponentName;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mFAB = (FloatingActionButton) findViewById(R.id.fab);
        mFAB.setOnClickListener(this);

        mTextViewSensibility = (TextView) findViewById(R.id.textViewSensibility);
        mSeekBar = (SeekBar) findViewById(R.id.seekBar);
        mSeekBar.setMax(20);
        mSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser)  {
                Log.i("onProgressChanged", "progress: "+progress);
                mTextViewSensibility.setText(getString(R.string.sensibility)+": "+(progress+1));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {}

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                MyService.sMaxAccelerationDuringShake = seekBar.getProgress() + 1;
                mTextViewSensibility.setText(getString(R.string.sensibility)+": "+MyService.sMaxAccelerationDuringShake);
            }
        });

        mNumberPicker = (HorizontalNumberPicker) findViewById(R.id.numberPicker);
        mNumberPicker.setMinValue(1);
        mNumberPicker.setMaxValue(10);
        mNumberPicker.setListener(new HorizontalNumberPickerListener() {
            @Override
            public void onHorizontalNumberPickerChanged(HorizontalNumberPicker horizontalNumberPicker, int value) {
                MyService.sMaxNumberOfShakes = value;
            }
        });

        mCheckBoxX = (CheckBox) findViewById(R.id.checkBoxX);
        mCheckBoxX.setOnCheckedChangeListener(this);
        mCheckBoxY = (CheckBox) findViewById(R.id.checkBoxY);
        mCheckBoxY.setOnCheckedChangeListener(this);
        mCheckBoxZ = (CheckBox) findViewById(R.id.checkBoxZ);
        mCheckBoxZ.setOnCheckedChangeListener(this);

        sDevicePolicyManager = (DevicePolicyManager) getSystemService(Context.DEVICE_POLICY_SERVICE);
        sComponentName = new ComponentName(this, MyDeviceAdminReceiver.class);
    }

    @Override
    protected void onResume() {
        super.onResume();
        updateUI();
    }

    @Override
    public void onClick(View view) {
        if (sDevicePolicyManager.isAdminActive(sComponentName)) {
            if (MyService.sIsRunning) {
                new MyServiceHandler().stop(this);
                setFABBackground(R.color.serviceStopped);
            } else {
                new MyServiceHandler().start(this);
                setFABBackground(R.color.serviceStarted);
            }
        } else {
            requestDeviceAdmin();
        }
    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
        if (!isChecked && !mCheckBoxX.isChecked() && !mCheckBoxY.isChecked() && !mCheckBoxZ.isChecked()) {
            Toast.makeText(this, R.string.you_cannot_disable_all_orientations, Toast.LENGTH_SHORT).show();
            compoundButton.setChecked(true);
            return;
        }
        if (compoundButton == mCheckBoxX) {
            MyService.sIsXEnabled = isChecked;
        } else if (compoundButton == mCheckBoxY) {
            MyService.sIsYEnabled = isChecked;
        } else if (compoundButton == mCheckBoxZ) {
            MyService.sIsZEnabled = isChecked;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.developer:
                showInformationDialog();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void requestDeviceAdmin() {
        Intent intent = new Intent(DevicePolicyManager.ACTION_ADD_DEVICE_ADMIN);
        intent.putExtra(DevicePolicyManager.EXTRA_DEVICE_ADMIN, sComponentName);
        startActivity(intent);
    }

    private void setFABBackground(int id) {
        mFAB.setBackgroundTintList(ColorStateList.valueOf(ContextCompat.getColor(this, id)));
    }

    private void updateUI() {
        mTextViewSensibility.setText(getString(R.string.sensibility)+": "+MyService.sMaxAccelerationDuringShake);
        mSeekBar.setProgress(MyService.sMaxAccelerationDuringShake);
        mNumberPicker.setValue(MyService.sMaxNumberOfShakes);
        mCheckBoxX.setChecked(MyService.sIsXEnabled);
        mCheckBoxY.setChecked(MyService.sIsYEnabled);
        mCheckBoxZ.setChecked(MyService.sIsZEnabled);

        if (sDevicePolicyManager.isAdminActive(sComponentName)) {
            if (MyService.sIsRunning) {
                setFABBackground(R.color.serviceStarted);
            } else {
                setFABBackground(R.color.serviceStopped);
            }
        }
    }

    private void showInformationDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        // Inflate and set the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout
        builder.setView(getLayoutInflater().inflate(R.layout.developer_information, null))
                // Add action buttons
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        // sign in the user ...
                        dialog.dismiss();
                    }
                });
        builder.create().show();
    }
}
